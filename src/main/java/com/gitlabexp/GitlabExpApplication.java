package com.gitlabexp;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.logging.Logger;

@SpringBootApplication
public class GitlabExpApplication {


	private static final Logger logger = (Logger) LoggerFactory.getLogger(GitlabExpApplication.class);
	@GetMapping(value = "/")
	public ResponseEntity<String> pong()
	{
		logger.info("Démarrage des services OK .....");
		return new ResponseEntity<String>("Réponse du serveur: "+ HttpStatus.OK.name(), HttpStatus.OK);
	}
	public static void main(String[] args) {
		SpringApplication.run(GitlabExpApplication.class, args);
	}

}
